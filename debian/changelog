tetzle (2.1.3+dfsg1-1) unstable; urgency=medium

  * New upstream version.
  * Bumped standards version to 4.1.4 (no changes required).
  * Migrated to Salsa.
  * Bumped debhelper and compat level to 11.
  * Updated years on d/copyright.
  * Updated README.source.
  * Added Files-Excluded field on d/copyright.
  * Added d/docs file.

 -- Innocent De Marchi <tangram.peces@gmail.com>  Mon, 11 Jun 2018 18:25:12 +0100

tetzle (2.1.2+dfsg1-1) unstable; urgency=medium

  * New upstream version.
  * Bumped standards version to 4.1.3 (no changes required).

 -- Innocent De Marchi <tangram.peces@gmail.com>  Sat, 06 Jan 2018 17:15:12 +0100

tetzle (2.1.1+dfsg1-2) unstable; urgency=medium

  * Changed libqt5opengl5-dev build dependency by libqt5opengl5-desktop-dev
    (Closes: #884395 ; LP: 935473).
  * Bumped standards version to 4.1.2 (no changes required).
  * Added tetrominoes word in keywords field on debian/desktop.

 -- Innocent De Marchi <tangram.peces@gmail.com>  Fri, 15 Dec 2017 17:13:25 +0100

tetzle (2.1.1+dfsg1-1) unstable; urgency=medium

  * New upstream release.
  * New maintainer (Closes: #737285).
    + Changed Maintainer field in debian/control file.
    + Add new maintainer in debian/copyright file.
  * Standards version 4.1.0: no special changes required.
  * Added Categories and Keywords fields on debian/desktop file.
  * Added tetzle.appdata.xml file on debian directory.
  * Reintroduced debian/watch file.
  * Added export DEB_BUILD_MAINT_OPTIONS on debian/rules.
  * Added Vcs-Browser and Vcs-Git on debian/control file.
  * Repacking sources due to unnecessary files.
    + Added README.source file en debian directory (see for
    details).
    + Added +dfsg1 to package name.

 -- Innocent De Marchi <tangram.peces@gmail.com>  Wed, 13 Sep 2017 07:23:17 +0100

tetzle (2.1.0-1) unstable; urgency=medium

  * QA upload.
  * New upstream release
    - Updated build-deps to QT5
  * Removed broken watch file
  * Replaced menu file with desktop file
  * Added some missing information in d/copyright
  * Bumped compat to 10
  * Standards version 4.0.0

 -- David William Richmond Davies-Jones <david@exultantmonkey.co.uk>  Thu, 29 Jun 2017 01:40:05 +0100

tetzle (2.0.3-1) unstable; urgency=medium

  * QA upload.
  * New upstream release
  * Removed fix-arm-build.patch - fixed upstream
  * Changed URLS to use HTTPS
  * Converted copyright to DEP-5
  * Bumped standards version to 3.9.8

 -- David William Richmond Jones <dwrj87@gmail.com>  Wed, 29 Jun 2016 06:39:28 +0100

tetzle (2.0.2.1-3) unstable; urgency=low

  * Fix "tetzle: build failures on ARM"
    - d/p/fix-arm-build.patch
      Thanks Graeme Gott for patch
    (Closes: #717949)

 -- Hector Oron <zumbi@debian.org>  Mon, 05 Aug 2013 00:48:34 +0000

tetzle (2.0.2.1-2) unstable; urgency=low

  * QA upload, orphaning this package.
  * New upstream release.

 -- Bart Martens <bartm@debian.org>  Fri, 26 Jul 2013 17:57:14 +0000

tetzle (2.0.1-1) unstable; urgency=low

  * New upstream release.  Closes: #668772.
  * debhelper 9, no more cdbs.
  * debian/patches/01_dont-draw-empty-scene.diff: Removed, is in upstream now.
  * debian/patches/02_check-for-GLSL.diff: Removed, is in upstream now.
  * debian/patches/03_detect-graphics-layer.diff: Removed, is in upstream now.

 -- Bart Martens <bartm@debian.org>  Sun, 20 May 2012 06:09:10 +0000

tetzle (2.0.0-2) unstable; urgency=low

  * Added patches.  Closes: #671738.
    + debian/patches/01_dont-draw-empty-scene.diff
    + debian/patches/02_check-for-GLSL.diff
    + debian/patches/03_detect-graphics-layer.diff
  * debian/control: Build-Conflicts: qt3-dev-tools.  Closes: #565796.

 -- Bart Martens <bartm@debian.org>  Sun, 13 May 2012 04:30:35 +0000

tetzle (2.0.0-1) unstable; urgency=low

  * Synced with https://launchpad.net/~gottcode/+archive/gcppa/
  * debian/control: Fixed "dpkg-source: warning: unknown information field
    'Suggests' in input data in general section of control info file".

 -- Bart Martens <bartm@debian.org>  Thu, 02 Jun 2011 02:51:09 +0000

tetzle (2.0.0-0ppa2~oneiric1) oneiric; urgency=low

  * Added optional dependency on jhead.

 -- Graeme Gott <graeme@gottcode.org>  Wed, 18 May 2011 20:31:13 -0400

tetzle (2.0.0-0ppa1~oneiric1) oneiric; urgency=low

  * New upstream release.

 -- Graeme Gott <graeme@gottcode.org>  Wed, 18 May 2011 14:12:30 -0400

tetzle (1.2.1-1) unstable; urgency=low

  * Initial package in Debian.  Closes: #530340.
  * debian/patches/01_paths.diff: Added.  Replaces /usr/local by /usr
    and /usr/bin by /usr/games.
  * debian/rules, debian/menu, debian/install: Use imagemagick to convert the
    icon to .xpm format for debian/menu.

 -- Bart Martens <bartm@debian.org>  Sun, 24 May 2009 00:19:45 +0200
